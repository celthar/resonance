package de.celthar.dungeon;

import java.awt.event.KeyEvent;

/**
 * Ein Objekt dieser Klasse stellt die Spielfigur dar, die gesteuert werden kann.
 * Dazu erbt die Klasse von AbstractEntity und implementiert somit die restlichen Methoden
 * des Interface Entity. Dazu werden weitere Spieler-spezifische Methoden implementiert.
 * 
 * @author Sascha
 *
 */
public final class Player extends AbstractEntity {
	private float nx = 0;
	private float ny = 0;
	private int health = 0;
	private boolean vulnerable = true;
	private long invincibleTime = 0;
	private long attackInterval = 0;
	private String facingDirection = "up";
	private Animation[] animations = new Animation[20];
	private Animation curAnimation = null;
	
	/**
	 * Erstellt ein Player-Objekt und weist diesem Animationen zu.
	 * @param ref Pfad zum Bild f�r den Spieler
	 * @param x x-Position, an der der Spieler erstellt werden soll
	 * @param y y-Position, an der der Spieler erstellt werden soll
	 * @param game Referenz auf das zugeh�rige Game-Objekt
	 * @param health Die Anzahl der Lebenspunkte, die der Spieler haben soll
	 */
	public Player(String ref, int x, int y, Game game, int health) {
		super(ref, x, y, game);
		this.health = health;
		
		/*
		 * Erstellen aller Animations-Objekte f�r den Spieler
		 */
		/*Spieler - stehend - oben*/
		Sprite[] sprites = {SpriteStore.get().getSprite("external/sprites/player_walking_up_1.gif")};
		animations[0] = new Animation(sprites, 1, 100);
		/*Spieler - stehend - unten*/
		Sprite[] sprites1 = {SpriteStore.get().getSprite("external/sprites/player_walking_down_1.gif")};
		animations[1] = new Animation(sprites1, 1, 100);
		/*Spieler - stehend - links*/
		Sprite[] sprites2 = {SpriteStore.get().getSprite("external/sprites/player_walking_left_1.gif")};
		animations[2] = new Animation(sprites2, 1, 100);
		/*Spieler - stehend - rechts*/
		Sprite[] sprites3 = {SpriteStore.get().getSprite("external/sprites/player_walking_right_1.gif")};
		animations[3] = new Animation(sprites3, 1, 100);
		/*Spieler - laufend - oben*/
		Sprite[] sprites4 = {SpriteStore.get().getSprite("external/sprites/player_walking_up_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_up_2.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_up_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_up_3.gif")};
		animations[4] = new Animation(sprites4, 4, 100);
		/*Spieler - laufend - unten*/
		Sprite[] sprites5 = {SpriteStore.get().getSprite("external/sprites/player_walking_down_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_down_2.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_down_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_down_3.gif")};
		animations[5] = new Animation(sprites5, 4, 100);
		/*Spieler - laufend - links*/
		Sprite[] sprites6 = {SpriteStore.get().getSprite("external/sprites/player_walking_left_1.gif"),SpriteStore.get().getSprite("external/sprites/player_walking_left_2.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_left_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_left_3.gif")};
		animations[6] = new Animation(sprites6, 4, 100);
		/*Spieler - laufend - rechts*/
		Sprite[] sprites7 = {SpriteStore.get().getSprite("external/sprites/player_walking_right_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_right_2.gif"),SpriteStore.get().getSprite("external/sprites/player_walking_right_1.gif"), SpriteStore.get().getSprite("external/sprites/player_walking_right_3.gif")};
		animations[7] = new Animation(sprites7, 4, 100);
		
		curAnimation = animations[0];
	}
	
	/**
	 * Gibt die aktuelle Zahl an Lebenpunkten zur�ck.
	 * @return momentane Lebenspunkte
	 */
	public int getHealth() {
		return health;
	}
	
	/**
	 * Logik-Methode f�r die Spielfigur
	 * @param delta Seit dem letzten Aufruf verstrichene Zeit
	 */
	public void tick(double delta) {
		/*
		 * Wenn der Spieler kein Leben mehr hat.
		 */
		if(health <= 0) {
			removeEntity();
			try {
				getGame().getHighScore().saveScore();
			} catch (Exception e) {
				e.printStackTrace();
			}
			getGame().gameOver();
		}
		
		invincibleTime += delta;
		attackInterval += delta;
		
		setSprite(curAnimation.nextSprite(delta));
		
		boolean attackPressed = InputHandler.getKey(KeyEvent.VK_F);
		if(attackPressed && attackInterval > 300) {
			attack();
			attackInterval = 0;
		}
		
		move();
	}
	
	/**
	 * Erstellt ein SwordEntity-Objekt, welches in Blickrichtung des Spielers bewegt wird,
	 * ausgehend von der momentanen Position des Spielers. (Dient als Angriff)
	 */
	public void attack() {
		getGame().addEntity(new SwordEntity((int) getX(), (int) getY(), facingDirection, getGame()));
	}
	
	/**
	 * Diese Methode k�mmert sich um die komplette Bewegungssteuerung der Spielfigur.
	 */
	public void move() {
		boolean up = InputHandler.getKey(KeyEvent.VK_W);
		boolean down = InputHandler.getKey(KeyEvent.VK_S);
		boolean left = InputHandler.getKey(KeyEvent.VK_A);
		boolean right = InputHandler.getKey(KeyEvent.VK_D);
		
		if(up) {
			ny = getY() - 2;
			if(validLocation(getX(), ny)) {
				setY(ny);
			}
			facingDirection = "up";
			curAnimation = animations[4];
		}
		if(down) {
			ny = getY() + 2;
			if(validLocation(getX(), ny)) {
				setY(ny);
			}
			facingDirection = "down";
			curAnimation = animations[5];
		}
		if(left) {
			nx = getX() - 2;
			if(validLocation(nx, getY())) {
				setX(nx);
			}
			facingDirection = "left";
			curAnimation = animations[6];
		}
		if(right) {
			nx = getX() + 2;
			if(validLocation(nx, getY())) {
				setX(nx);
			}
			facingDirection = "right";
			curAnimation = animations[7];
		}
		
		if(!up && !down && ! left && ! right) {
			if(facingDirection.equals("up")) {
				curAnimation = animations[0];
			} else if(facingDirection.equals("down")) {
				curAnimation = animations[1];
			} else if(facingDirection.equals("left")) {
				curAnimation = animations[2];
			} else if(facingDirection.equals("right")) {
				curAnimation = animations[3];
			}
		}
		
		int index;
		if((index = reachedBounds()) != 0) {
			getGame().getLevel().changeRoom(index);
		}
	}
	
	/**
	 * Diese Methode pr�ft, ob die Spielfigur den Rand des Spielfelds �bertritt,
	 * setzt den Spieler an die gegen�berliegende Seite und gibt die Room-ID Differenz
	 * zur�ck.
	 * @return Differenz der neuen Room-ID zur alten Room-ID
	 */
	public int reachedBounds() {
		if(getX() <= 0) {
			setX(864);
			return -1;
		}
		if(getX() >= 896) {
			setX(32);
			return +1;
		}
		if(getY() <= 0) {
			setY(608);
			return -10;
		}
		if(getY() >= 640) {
			setY(32);
			return +10;
		}
		
		return 0;
	}

	/**
	 * Diese Methode f�hrt bei einer Kollision mit einer anderen Entit�t
	 * entsprechende Ma�nahmen durch.
	 * 
	 * Kollision mit einem <i>Enemy</i>-Objekt:
	 * - Leben verlieren
	 * - HighScore verringern
	 * - kurzzeitig unverwundbar machen
	 */
	public void collidedWith(Entity other) {
		if(other instanceof Enemy) {
			if(invincibleTime > 500) {
				vulnerable = true;
			}
			if(vulnerable) {
				this.health -= 1;
				getGame().getHighScore().decreasePoints(1);
				vulnerable = false;
				invincibleTime = 0;
			}
		}
	}
}
