package de.celthar.dungeon;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

/**
 * Ein Objekt dieser Klasse stellt einen AudioClip dar.
 * 
 * @author Sascha
 *
 */
public class Audio implements AudioClip {
	AudioClip audio;
	
	/**
	 * Erstellt ein AudioClip-Objekt, welches eine Audio-Datei aus dem Verzeichnis
	 * "resources/music/" l�dt.
	 * @param file Der Dateiname der Audio-Datei, die gelaaden werden soll
	 */
	public Audio(String file) {
		URL url = this.getClass().getClassLoader().getResource("external/audio/" + file);
		this.audio = Applet.newAudioClip(url);
	}
	
	/**
	 * ruft die loop()-Methode des AudioClip-Objekts auf.
	 */
	@Override
	public void loop() {
		audio.loop();
	}
	
	/**
	 * ruft die play()-Methode des AudioClip-Objekts auf.
	 */
	@Override
	public void play() {
		audio.play();
	}

	/**
	 * ruft die stop()-Methode des AudioClip-Objekts auf.
	 */
	@Override
	public void stop() {
		audio.stop();
	}

}
