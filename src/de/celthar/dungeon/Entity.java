package de.celthar.dungeon;

import java.awt.Graphics;

/**
 * Stellt ein Interface f�r Entit�ten zur Verf�gung. Entit�ten sind Objekte, die interagieren,
 * reagieren und sich bewegen k�nnen.
 * 
 * @author Sascha
 *
 */
interface Entity {
	/**
	 * Gibt den x-Wert der Entit�t zur�ck.
	 * @return x-Position
	 */
	public float getX();
	
	/**
	 * Gibt den y-Wert der Entit�t zur�ck.
	 * @return y-Position
	 */
	public float getY();
	
	/**
	 * Liefert die Referenz auf das zugeh�rige Game-Objekt.
	 * @return Das Game-Objekt, in dem diese Entit�t existiert
	 */
	public Game getGame();
	
	/**
	 * Setzt die x-Position der Entit�t.
	 * @param x Absoluter x-Wert (linke obere Ecke)
	 */
	public void setX(float x);
	
	/**
	 * Setzt die y-Positionn der Entit�t.
	 * @param y Absoluter y-Wert (linke obere Ecke)
	 */
	public void setY(float y);
	
	/**
	 * Zeichnet das Objekt auf dem Grafikkontext
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g);
	
	/**
	 * Logik-Methode der Entit�t.
	 * @param delta Seit dem letzten Aufruf verstrichene Zeit in Millisekunden
	 */
	public void tick(double delta);
	
	/**
	 * Bewegen-Methode der Entit�t.
	 */
	public void move();
	
	/**
	 * Entfernt die Entit�t aus dem Spiel.
	 */
	public void removeEntity();
	
	/**
	 * Pr�ft, ob die Entit�t mit einer anderen Entit�t kollidiert.
	 * @param other Die Entit�t mit der die Kollision gepr�ft werden soll
	 * @return true, wenn die Enntit�ten kollidieren, sonst false
	 */
	public boolean collidesWith(AbstractEntity other);
	
	/**
	 * F�hrt entsprechende Methoden aus, je nachdem mit welchem Objekt-Typ
	 * die Entit�t kollidiert ist.
	 * @param other Die Entit�t mit der das Objekt kollidiert ist.
	 */
	public void collidedWith(Entity other);
}
