package de.celthar.dungeon;

import java.awt.Graphics;

/**
 * Implementierung eines Menu-Objekts.
 * 
 * @author Sascha
 *
 */
public final class PauseMenu implements Menu {
	/**
	 * Implementierung der Zeichen-Methode des Interfaces.
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g) {
		SpriteStore.get().getSprite("external/sprites/pauseMenu.gif").draw(g, 0, 0);
	}

	/**
	 * Implementierung der Logik des GameOverMenu-Objekts (momentan ungenutzt)
	 */
	public void tick() {
	}
}
