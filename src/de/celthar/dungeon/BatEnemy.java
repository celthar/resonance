package de.celthar.dungeon;

import java.util.Random;

/**
 * Diese Klasse stellt eine Fledermaus als Gegner dar.
 * 
 * @author Sascha
 *
 */
public final class BatEnemy extends Enemy {
	private boolean vulnerable = true;
	private long invincibleTime = 0;
	private Animation curAnimation = null;
	Random rand = new Random();
	
	/**
	 * Erstellt ein BatEnemy-Objekt.
	 * @param x x-Position des Objekts (linke obere Ecke)
	 * @param y y-Position des objekts (linke obere Ecke)
	 * @param game Das zugeh�rige Game-Objekt
	 */
	public BatEnemy(int x, int y, Game game) {
		super("external/sprites/bat.gif", x, y, game, 2);
		Sprite[] sprites = {SpriteStore.get().getSprite("external/sprites/bat.gif"), SpriteStore.get().getSprite("external/sprites/bat_flying.gif")};
		curAnimation = new Animation(sprites, 2, 100);
	}

	/**
	 * Logik-Methode des BatEnemy-Objekts
	 * @param delta Die seit dem letzten Aufruf verstrichene Zeit
	 */
	@Override
	public void tick(double delta) {
		if(getHealth() <= 0) {
			removeEntity();
			getGame().getHighScore().increasePoints(1);
		}
		
		invincibleTime += delta;
		
		setSprite(curAnimation.nextSprite(delta));
		
		move();
	}
	
	/**
	 * Die Bewegungs-Methode des BatEnemy-Objekts.
	 */
	@Override
	public void move() {
		int nx = (int) (getX() + (rand.nextInt() % 3));
		int ny = (int) (getY() + (rand.nextInt() % 3));
		
		if(nx >= 896) {
			nx = 896;
		}
		if(nx <= 32) {
			nx = 32;
		}
		if(ny >= 640) {
			ny = 640;
		}
		if(ny <= 32) {
			ny = 32;
		}
		
		if(validLocation(nx, getY())) {
			setX(nx);
		}
		if(validLocation(getX(), ny)) {
			setY(ny);
		}
	}
	
	/**
	 * Diese Methode f�hrt bei einer Kollision mit einer anderen Entit�t
	 * entsprechende Ma�nahmen durch.
	 * 
	 * Kollision mit einem <i>SwordEntity</i>-Objekt:
	 * - Leben verlieren
	 * - kurzzeitig unverwundbar machen
	 */
	public void collidedWith(Entity other) {
		if(other instanceof SwordEntity) {
			if(invincibleTime > 300) {
				vulnerable = true;
			}
			if(vulnerable) {
				setHealth(getHealth() - 1);
				vulnerable = false;
				invincibleTime = 0;
			}
		}
	}
}
