package de.celthar.dungeon;

import java.util.Random;

/**
 * Ein Objekt dieser Klasse stellt eine Spinne als Gegner dar.
 * 
 * @author Sascha
 *
 */
public final class SpiderEnemy extends Enemy {
	private boolean vulnerable = true;
	private long invincibleTime = 0;
	private Animation curAnimation;
	Random rand = new Random();
	private int moveDirection;
	private int moveWidth = 0;
	private int moved = 0;
	
	/**
	 * Erstellt das SpiderEnemy-Objekt
	 * @param x x-Position des Objekts (linke obere Ecke)
	 * @param y y-Position des objekts (linke obere Ecke)
	 * @param game Das zugeh�rige Game-Objekt
	 */
	public SpiderEnemy(int x, int y, Game game) {
		super("external/sprites/spider.gif", x, y, game, 3);
		Sprite[] sprites = {SpriteStore.get().getSprite("external/sprites/spider.gif"), SpriteStore.get().getSprite("external/sprites/spider_walking_1.gif"), SpriteStore.get().getSprite("external/sprites/spider.gif"), SpriteStore.get().getSprite("external/sprites/spider_walking_2.gif")};
		curAnimation = new Animation(sprites, 4, 100);
	}

	/**
	 * Logik-Methode des SpiderEnemy-Objekts.
	 */
	public void tick(double delta) {
		if(getHealth() <= 0) {
			removeEntity();
			getGame().getHighScore().increasePoints(2);
		}
		
		invincibleTime += delta;
		
		setSprite(curAnimation.nextSprite(delta));
		
		move();
	}

	/**
	 * Methode zum Bewegen dieses Objekts.
	 */
	public void move() {
		if(moved >= moveWidth) {		
			int tmp = Math.abs(rand.nextInt() % 4);
			
			switch(tmp) {
				case 0:
					/*Oben*/
					moveDirection = 0;
					moveWidth = Math.abs(rand.nextInt() % 100);
					moved = 0;
					break;
				case 1:
					/*Unten*/
					moveDirection = 1;
					moveWidth = Math.abs(rand.nextInt() % 100);
					moved = 0;
					break;
				case 2:
					/*Links*/
					moveDirection = 2;
					moveWidth = Math.abs(rand.nextInt() % 100);
					moved = 0;
					break;
				case 3:
					/*Rechts*/
					moveDirection = 3;
					moveWidth = Math.abs(rand.nextInt() % 100);
					moved = 0;
					break;
			}
		}
		
		int nx = (int) getX();
		int ny = (int) getY();
		
		if(moveDirection == 0) {
			ny = (int) (getY() - 1);
		}
		if(moveDirection == 1) {
			ny = (int) (getY() + 1);
		}
		if(moveDirection == 2) {
			nx = (int) (getX() - 1);
		}
		if(moveDirection == 3) {
			nx = (int) (getX() + 1);
		}
		
		moved += 1;
		
		if(nx >= 895) {
			nx = 895;
		}
		if(nx <= 32) {
			nx = 32;
		}
		if(ny >= 640) {
			ny = 640;
		}
		if(ny <= 32) {
			ny = 32;
		}
		
		if(validLocation(nx, getY())) {
			setX(nx);
		}
		if(validLocation(getX(), ny)) {
			setY(ny);
		}
		
		/*
		int nx = (int) (getX() + (rand.nextInt() % 10));
		int ny = (int) (getY() + (rand.nextInt() % 10));
		
		if(nx >= 896) {
			nx = 896;
		}
		if(nx <= 32) {
			nx = 32;
		}
		if(ny >= 640) {
			ny = 640;
		}
		if(ny <= 32) {
			ny = 32;
		}
		
		if(validLocation(nx, getY())) {
			setX(nx);
		}
		if(validLocation(getX(), ny)) {
			setY(ny);
		}
		*/
	}

	/**
	 * Diese Methode f�hrt bei einer Kollision mit einer anderen Entit�t
	 * entsprechende Ma�nahmen durch.
	 * 
	 * Kollision mit einem <i>SwordEntity</i>-Objekt:
	 * - Leben verlieren
	 * - kurzzeitig unverwundbar machen
	 */
	public void collidedWith(Entity other) {
		if(other instanceof SwordEntity) {
			if(invincibleTime > 300) {
				vulnerable = true;
			}
			if(vulnerable) {
				setHealth(getHealth() - 1);
				vulnerable = false;
				invincibleTime = 0;
			}
		}
	}
}
