package de.celthar.dungeon;

/**
 * Diese Klasse stellt einen Schwierigkeits-Modifikator dar.
 * 
 * @author Sascha
 *
 */
public final class Difficulty {
	private double difficulty = 0;
	private double inc = 0;
	
	/**
	 * Erstellt das Difficulty-Objekt mit einer Standard-Schwierigkeit von 1.00
	 * @param inc Der Wert, um den die Schwierigkeit erh�ht werden soll
	 */
	public Difficulty(double inc) {
		difficulty = 1.00;
		this.inc = inc;
	}
	
	/**
	 * Erh�ht die Schwierigkeit mit der Formel:
	 * <i>difficulty = difficulty + inc</i>
	 */
	public void increaseDifficultyAdd() {
		difficulty += inc;
	}
	
	/** 
	 * Erh�ht die Schwierigkeit mit der Formel:
	 * <i>difficulty = difficulty + (difficulty * inc)</i>
	 */
	public void increaseDifficultyMul() {
		difficulty += (difficulty * inc);
	}
	
	/**
	 * Gibt die momentane Schwierigkeit zur�ck.
	 * @return Die momentane Schwierigkeit
	 */
	public double getDifficulty() {
		return difficulty;
	}

	/**
	 * Setzt die Schwierigkeit wieder auf  1.00 zur�ck.
	 */
	public void resetDifficulty() {
		difficulty = 1.00;
	}
}
