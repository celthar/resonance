package de.celthar.dungeon;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;

/**
 * Ein Objekt dieser Klasse repr�sentiert das komplette Spiel, inklusive aller erforderlichen Logik
 * und Objekte. Diese Klasse ist auch der Einstiegspunkt f�r das Spiel, somit enth�lt sie die
 * erforderliche main-Methode zum Starten des Spiels.
 * 
 * @author Sascha
 *
 */

@SuppressWarnings("serial")
public class Game extends Canvas {
	private BufferStrategy bs = null;
	private boolean gameRunning = false;
	private Difficulty difficulty = new Difficulty(0.5);
	private long difTime = 0;
	private String playerName = "";
	private Sprite background = SpriteStore.get().getSprite("external/sprites/background.gif");
	private Audio bgm = new Audio("bgm.wav");
	private static ArrayList<Entity> entityList = new ArrayList<Entity>();
	private static ArrayList<Entity> removeList = new ArrayList<Entity>();
	private Level level = new Level(this);
	private Menu menu = new TitleMenu();
	private HighScore highscore = new HighScore();

	/**
	 * Erstellt den JFrame samt aller erforderlichen Elemente des JFrames.
	 */
	public Game() {
		JFrame frame = new JFrame("Resonance v1.0");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = (JPanel) frame.getContentPane();
		panel.setPreferredSize(new Dimension(928 + 128, 672));
		this.setBounds(0, 0, 928 + 128, 672);
		panel.add(this);

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = null;
		JMenuItem menuItem = null;
		ActionHandler al = new ActionHandler(this);
		
		menu = new JMenu("Game");
		menuItem = new JMenuItem("New Game");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuItem = new JMenuItem("Pause Game");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuItem = new JMenuItem("Exit Game");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuBar.add(menu);
		
		menu = new JMenu("HighScore");
		menuItem = new JMenuItem("Show ScoreBoard");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuBar.add(menu);
		
		menu = new JMenu("Help");
		menuItem = new JMenuItem("Controls");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuItem = new JMenuItem("About Game");
		menuItem.addActionListener(al);
		menu.add(menuItem);
		menuBar.add(menu);
		
		frame.setJMenuBar(menuBar);
		
		this.setIgnoreRepaint(true);
		
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);
		
		this.addKeyListener(new InputHandler());
		this.requestFocus();

		this.createBufferStrategy(2);
		bs = getBufferStrategy();
	}
	
	/**
	 * @return Das momentane Level-Objekt.
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * @return Das momentane Menu-Objekt.
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * @return Das momentane HighScore-Objekt.
	 */
	public HighScore getHighScore() {
		return highscore;
	}

	/**
	 * Initialisiert alle Entit�ten des Spiels
	 */
	public void initEntities() {
		Entity player = new Player("external/sprites/player_walking_up_1.gif", 464, 336, this, 10);
		addEntity(player);
		
		initEnemies();
	}
	
	/**
	 * F�gt dem Spiel ein Entity-Objekt hinzu.
	 * @param entity Das hinzuzuf�gende Entity-Objekt
	 */
	public void addEntity(Entity entity) {
		entityList.add(entity);
	}
	
	/**
	 * Entfernt ein Entity-Objekt aus dem Spiel.
	 * @param entity Das zu entfernende Entity-Objekt
	 */
	public void removeEntity(Entity entity) {
		removeList.add(entity);
	}

	/**
	 * Initialisiert die Enemy-Objekte
	 */
	public void initEnemies() {
		Random rand = new Random();
		
		/* TODO: Fehler: Gegner werden an unm�glichen Orten erstellt. */
		for(int x = 1; x <= (3 * difficulty.getDifficulty()); x++) {
			Entity enemy = new BatEnemy(Math.abs((rand.nextInt() % 27) * 32) + 32, Math.abs((rand.nextInt() % 19) * 32) + 32, this);
			if(((AbstractEntity) enemy).validLocation(enemy.getX(), enemy.getY())) {
				addEntity(enemy);
			} else {
				enemy = null;
				x--;
			}
		}
		for(int x = 1; x <= (2 * difficulty.getDifficulty()); x++) {
			Entity enemy = new SpiderEnemy(Math.abs((rand.nextInt() % 27) * 32) + 32, Math.abs((rand.nextInt() % 19) * 32) + 32, this);
			if(((AbstractEntity) enemy).validLocation(enemy.getX(), enemy.getY())) {
				addEntity(enemy);
			} else {
				enemy = null;
				x--;
			}
		}
	}
	
	/**
	 * Entfernt alle Enemy-Objekte aus dem Spiel.
	 */
	public void removeEnemies() {
		for(int x = 0; x < entityList.size(); x++) {
			if(entityList.get(x) instanceof Enemy) {
				entityList.remove(x);
			}
		}
		removeList.clear();
	}

	/**
	 * Setzt alle relevanten Variablen auf Standardwerte um ein neues Spiel zu erm�glichen.
	 */
	public void newGame() {
		menu = new TitleMenu();
		entityList.clear();
		removeList.clear();
		difficulty.resetDifficulty();
		level = new Level(this);
		highscore = new HighScore(playerName);
		this.initEntities();
	}

	/**
	 * Setzt Variablen und startet Methoden f�r den ersten Spielstart.
	 */
	public void startGame() {
		menu = new TitleMenu();
		playerName = JOptionPane.showInputDialog("Name:");
		highscore = new HighScore(playerName);
		this.initEntities();
		bgm.loop();
		gameRunning = true;
		this.gameLoop();
	}
	
	/**
	 * Pausiert das Spiel.
	 */
	public void pauseGame() {
		menu = new PauseMenu();
	}
	
	/**
	 * Beendet das Spiel.
	 */
	public void exitGame() {
		System.exit(0);
	}
	
	/**
	 * Zeigt einen GameOver-Bildschirm an.
	 */
	public void gameOver() {
		menu = new GameOverMenu();
	}
	
	/**
	 * Methode zum Aufrufen der Zeichen- und Logik-Methoden der relevanten Objekte.
	 */
	public void gameLoop() {
		long lastLoopTime = System.currentTimeMillis();
		while(gameRunning) {
			long delta = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();
			
			Graphics2D g = (Graphics2D) bs.getDrawGraphics();
			
			/*
			 * Entfernen aller Entit�ten, die zur removeList hinzugef�gt wurden.
			 */
			entityList.removeAll(removeList);
			removeList.clear();
			
			boolean menuToggle = InputHandler.getKey(KeyEvent.VK_SPACE);
			boolean pauseToggle = InputHandler.getKey(KeyEvent.VK_ESCAPE);
			
			if(menuToggle && menu != null) {
				if(!(menu instanceof GameOverMenu)) {
					menu = null;
				}
			}
			if(pauseToggle && menu == null) {
				pauseGame();
			}
			
			if(menu != null) {
				menu.draw(g);
			} else {
				difTime += delta;
				if(difTime >= 30000) {
					difficulty.increaseDifficultyMul();
					difTime = 0;
				}
				
				level.draw(g);
				background.draw(g, 928, 0);
				g.drawString(playerName, 934, 15);
				g.drawString("Points: " + String.valueOf(highscore.getPoints()), 934, 45);
				
				/*
				 * Zeichnen und Logik der Entit�ten
				 */
				for(int x = 0; x < entityList.size(); x++) {
					Entity entity = (Entity) entityList.get(x);
					entity.draw(g);
					entity.tick(delta);
					
					if(entity instanceof Player) {
						g.drawString("Health: " + String.valueOf(((Player) entityList.get(0)).getHealth()), 934, 30);
					}
					
					/*
					 * Kollisions-Erkennung
					 */
					for(int y = x + 1; y < entityList.size(); y++) {
						AbstractEntity entity2 = (AbstractEntity) entityList.get(y);
						if(entity.collidesWith(entity2)) {
							entity.collidedWith(entity2);
							entity2.collidedWith(entity);
						}
					}
				}
			}
			
			g.dispose();
			bs.show();
			
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}
		}	
	}
	
	/**
	 * Einstiegspunkt f�r das Spiel.
	 * @param args
	 */
	public static void main(String[] args) {
		Game game = new Game();
		game.startGame();
	}
}
