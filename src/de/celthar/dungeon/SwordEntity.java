package de.celthar.dungeon;

/**
 * Ein Objekt dieser Klasse stellt ein Schwert dar, mit dem der Spieler
 * angreift und Gegner verletzt werden.
 * 
 * @author Sascha
 *
 */
public final class SwordEntity extends AbstractEntity {
	private int ox = 0;
	private int oy = 0;
	private String movingDirection = "up";
	
	/**
	 * Erstellt ein SwordEntity-Objekt.
	 * @param x Die x-Position, an der das Objekt urspr�nglich erstellt wird
	 * @param y Die y-Position, an der das Objekt urspr�nglich erstellt wird
	 * @param facingDirection Die Richtung, in die das Schwert bewegt werden soll ("up", "down", "left" oder "right")
	 * @param game Eine Referenz auf das zugeh�rige Game-Objekt
	 */
	public SwordEntity(int x, int y, String facingDirection, Game game) {
		super("external/sprites/sword_" + facingDirection + ".gif", x, y, game);
		this.ox = x;
		this.oy = y;
		this.movingDirection = facingDirection;
	}

	/**
	 * Logik-Methode f�r das SwordEntity-Objekt
	 */
	public void tick(double delta) {
		move();
	}

	/**
	 * Bewegungs-Methode f�r das SwordEntity-Objekt. Das Objekt wird von seiner urspr�nglichen
	 * Position aus um 32 Pixel bewegt, in die Richtung, in die der Spieler geschaut hat.
	 */
	public void move() {
		if(movingDirection.equals("up")) {
			setY(getY() - 5);
		} else if(movingDirection.equals("down")) {
			setY(getY() + 5);
		} else if(movingDirection.equals("left")) {
			setX(getX() - 5);
		} else if(movingDirection.equals("right")) {
			setX(getX() + 5);
		}		
		if(getX() < ox - 32 || getY() < oy - 32 || getX() > ox + 32 || getY() > oy + 32) {
			removeEntity();
		}
	}

	/**
	 * Durch das Interface <i>Entity</i> erforderliche, in diesem Fall aber nicht genutzte Methode.
	 */
	public void collidedWith(Entity other) {
	}
}
