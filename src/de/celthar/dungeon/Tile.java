package de.celthar.dungeon;

import java.awt.Graphics;

/**
 * Ein Objekt dieser Klasse stellt eine "Fliese" eines Room-Objekts dar. (Ein 32 * 32 Pixel
 * breites Quadrat)
 * 
 * @author Sascha
 *
 */
public final class Tile {
	private Sprite sprite = null;
	private boolean collision = false;
	
	/**
	 * Erstellt ein Tile-Objekt mit dem entsprechenden Bild und Kollisons-Wert.
	 * @param ref Pfad zum Bild
	 * @param collision Kollisionswert (true, wenn das Feld nicht betreten werden darf; sonst false)
	 */
	public Tile(String ref, boolean collision) {
		this.sprite = SpriteStore.get().getSprite(ref);
		this.collision = collision;
	}
	
	/**
	 * Zeichnet das Tile-Objekt.
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 * @param x Die x-Position, an der gezeichnet werden soll
	 * @param y Die y-Position, an der gezeichnet werden soll
	 */
	public void draw(Graphics g, int x, int y) {
		sprite.draw(g, x, y);
	}
	
	/**
	 * Gibt den Kollisionswert zur�ck.
	 * @return true, wenn der Kollisionswert true ist; sonst false
	 */
	public boolean getCollision() {
		return collision;
	}
}
