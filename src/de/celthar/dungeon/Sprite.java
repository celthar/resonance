package de.celthar.dungeon;

import java.awt.Graphics;
import java.awt.Image;

/**
 * Ein Objekt dieser Klasse stellt ein Bild dar, welches gezeichnet werden kann.
 * 
 * @author Sascha
 *
 */
public final class Sprite {
	private Image image = null;
	
	/**
	 * Erstellt ein Sprite-Objekt aus einem Image-Objekt.
	 * @param image Das Image-Objekt, welches f�r den Sprite verwendet werden soll
	 */
	public Sprite(Image image) {
		this.image = image;
	}
	
	/**
	 * Gibt die Breite des verwendeten Image-Objekts zur�ck.
	 * @return Breite des Bildes
	 */
	public int getWidth() {
		return image.getWidth(null);
	}
	
	/**
	 * Gibt die H�he des verwendeten Image-Objekts zur�ck.
	 * @return H�he des Bildes
	 */
	public int getHeight() {
		return image.getHeight(null);
	}
	
	/**
	 * Zeichnet das Bild
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 * @param x Die x-Position, an der gezeichnet werden soll
	 * @param y Die y-Position, an der gezeichnet werden soll
	 */
	public void draw(Graphics g, int x, int y) {
		g.drawImage(image, x, y, null);
	}
}
