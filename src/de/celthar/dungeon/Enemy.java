package de.celthar.dungeon;

/**
 * Abstrakte Klasse um weitere Methoden des Entity-Interfaces zu implementieren, die
 * nur f�r Gegner ben�tigt werden.
 * 
 * @author Sascha
 *
 */
public abstract class Enemy extends AbstractEntity {
	private int health = 0;
	
	/**
	 * Dient f�r die Unterklassen als gemeinsamer Konstruktor.
	 * @param ref Pfad zur Bild-Datei
	 * @param x x-Position des Objekts
	 * @param y y-Position des Objekts
	 * @param game Referenz auf das zugeh�rige Game-Objekt
	 * @param health Lebenspunkte des Enemy-Objekts
	 */
	public Enemy(String ref, int x, int y, Game game, int health) {
		super(ref, x, y, game);
		this.health = health;
	}
	
	/**
	 * Setzt die Lebenspunkte des Enemy-Objekts.
	 * @param health Der Wert, auf den die Lebenspunkte gesetzt werden sollen
	 */
	public void setHealth(int health) {
		this.health = health;
	}
	
	/**
	 * @return Die momentanen Lebenspunkte
	 */
	public int getHealth() {
		return health;
	}
}
