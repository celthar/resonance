package de.celthar.dungeon;

import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Ein Objekt dieser Klasse stellt einen Raum im Spiel dar. Ein Raum besteht aus
 * 29 * 21 Tile-Objekten, welche in einem zwei-dimensionalen Array gespeichert sind.
 * 
 * @author Sascha
 *
 */
public final class Room {
	private final static int WIDTH = 29;
	private final static int HEIGHT = 21;
	private Tile[][] map = new Tile[WIDTH][HEIGHT];
	
	/**
	 * Erstellt ein Room-Objekt, welches alle Tile-Objekte entsprechend der
	 * jeweiligen Datei entnimmt. (index => room_index.map)
	 * @param index Index des Raumes, der geladen werden soll
	 * @throws URISyntaxException Aufgrund der internen Verarbeitung notwendig, sollte niemals auftreten
	 * @throws IOException Tritt auf, wenn die Datei nicht ge�ffnet werden kann
	 */
	public Room(int index) throws URISyntaxException, IOException {
		/*
		 * Laden der entsprechenden Datei
		 */
		URL url = this.getClass().getClassLoader().getResource("external/maps/room_" + index + ".map");
		File f = new File(url.toURI());
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = "";
		
		/*
		 * Einlesen der Datei
		 */
		int y = 0;
		while((line = br.readLine()) != null) {
			String[] tiles = line.split(";");
			
			/*
			 * Verarbeitung der Werte aus der Datei zu Tile-Objekten
			 */
			for(int x = 0; x < tiles.length; x++) {
				if(tiles[x].equals("0")) {
					map[x][y] = new Tile("external/sprites/stone.gif", true);
				} else if(tiles[x].equals("1")) {
					map[x][y] = new Tile("external/sprites/grass.gif", false);
				} else if(tiles[x].equals("2")) {
					map[x][y] = new Tile("external/sprites/sand.gif", false);
				} else if(tiles[x].equals("3")) {
					map[x][y] = new Tile("external/sprites/water.gif", true);
				} else if(tiles[x].equals("4")) {
					map[x][y] = new Tile("external/sprites/wall.gif", true);
				} else {
					map[x][y] = new Tile("external/sprites/blank.gif", false);
				}
			}
			y++;
		}
		br.close();
	}
	
	/**
	 * Ruft die <i>draw(g)</i>-Methode der einzelnen Tile-Objekte auf.
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g) {
		for(int x = 0; x < WIDTH; x++) {
			for(int y = 0; y < HEIGHT; y++) {
				map[x][y].draw(g, x*32, y*32);
			}
		}
	}
	
	/**
	 * Gibt den boolean-Wert f�r die Kollision an der entsprechenden (x,y)-Stelle
	 * des Arrays zur�ck.
	 * @param x x-Stelle im Array
	 * @param y y-Stelle im Array
	 * @return true, wenn das Feld nicht betreten werden soll; sonst false
	 */
	public boolean getCollision(int x, int y) {
		return map[x][y].getCollision();
	}
}
