package de.celthar.dungeon;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * Implementiert einen Teil der Methoden f�r Entity-Objekte, die �ber alle Unterklassen
 * gleich funktionieren.
 * 
 * @author Sascha
 *
 */
public abstract class AbstractEntity implements Entity {
	private float x = 0;
	private float y = 0;
	private Sprite sprite = null;
	private Game game = null;
	private Rectangle me = new Rectangle();
	private Rectangle him = new Rectangle();
	
	/**
	 * Dient f�r die Unterklassen als gemeinsamer Konstruktor.
	 * @param ref Pfad zur Bild-Datei
	 * @param x Position des Objekts auf der x-Achse (linke obere Ecke)
	 * @param y Position des Objekts auf der y-Achse (linke obere Ecke)
	 * @param game Referenz auf das zugeh�rige Game-Objekt
	 */
	public AbstractEntity(String ref, int x, int y, Game game) {
		this.sprite = SpriteStore.get().getSprite(ref);
		this.x = x;
		this.y = y;
		this.game = game;
	}
	
	/**
	 * Zeichnet das Bild auf dem �bergebenen Grafikkontext.
	 * @param g Der Grafikkontext auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g) {
		sprite.draw(g, (int) x, (int) y);
	}
	
	/**
	 * Gibt die Position auf der x-Achse zur�ck.
	 * @return x-Wert des Objekts
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * Gibt die Position auf der y-Achse zur�ck.
	 * @return y-Wert des Objekts
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Gibt die Referenz auf das zugeh�rige Game-Objekt zur�ck.
	 * @return Game-Objekt der Entit�t
	 */
	public Game getGame() {
		return game;
	}
	
	/**
	 * Setzt den x-Wert des Objekts.
	 * @param x Absoluter x-Wert
	 */
	public void setX(float x) {
		this.x = x;
	}
	
	/**
	 * Setzt den y-Wert des Objekts.
	 * @param y Absoluter y-Wert
	 */
	public void setY(float y) {
		this.y = y;
	}
	
	/**
	 * Setzt das Bild der Entit�t.
	 * @param sprite Das Bild als Sprite-Objekt
	 */
	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	
	/**
	 * Entfernt das Entity-Objekt aus dem Spiel.
	 */
	public void removeEntity() {
		game.removeEntity(this);
	}
	
	/**
	 * Pr�ft ob die Position, in die das Entity-Objekt bewegt werden soll, blockt.
	 * @param x x-Position des Entity-Objekts
	 * @param y y-Position des Entity-Objekts
	 * @return true wenn die Position frei ist, sonst false
	 */
	public boolean validLocation(float x, float y) {
		if(game.getLevel().getCurrentRoom().getCollision((int) (x / 32), (int) (y / 32))) {
			return false;
		}
		if(game.getLevel().getCurrentRoom().getCollision((int) (x / 32), (int) ((y + sprite.getHeight() - 1) / 32))) {
			return false;
		}
		if(game.getLevel().getCurrentRoom().getCollision((int) ((x + sprite.getWidth() - 1) / 32), (int) (y / 32))) {
			return false;
		}
		if(game.getLevel().getCurrentRoom().getCollision((int) ((x + sprite.getWidth() - 1) / 32), (int) ((y + sprite.getHeight() - 1) / 32))) {
			return false;
		}
		
		return true;
	}

	/**
	 * Pr�ft, ob zwei Entit�ten miteinander zusammensto�en.
	 * @param other Ein AbstractEntity-Objekt, das mit diesem Objekt auf Kollision gepr�ft wird.
	 * @return true, wenn die Objekte kollidieren, sonst false
	 */
	public boolean collidesWith(AbstractEntity other) {
		me.setBounds((int) x, (int) y, sprite.getWidth(), sprite.getHeight());
		him.setBounds((int) other.x, (int) other.y, other.sprite.getWidth(), other.sprite.getHeight());
		
		return me.intersects(him);
	}
}
