package de.celthar.dungeon;

import java.awt.Graphics;

/**
 * Ein Objekt dieser Klasse stellt das komplette Level dar. Ein Level besteht aus
 * mehreren Room-Objekten. Das Level-Objekt k�mmert sich vor allem um den reibungslosen
 * �bergang zwischen den Room-Objekten
 * 
 * @author Sascha
 *
 */
public final class Level {
	private Game game = null;
	private Room curRoom = null;
	private int curRoomIndex = 0;

	/**
	 * Erstellt ein Level-Objekt mit einer Referenz auf das zugeh�rige Game-Objekt.
	 * Der Anfangsraum wird �ber <i>new Room(0);</i> initialisiert.
	 * @param game Referenz auf ein Game-Objekt
	 */
	public Level(Game game) {
		this.game = game;
		
		try {
			curRoom = new Room(0);
			curRoomIndex = 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Verantwortlich f�r das ordnungsgem��e Wechseln der R�ume.
	 * @param index Der Wert, um den die Room-ID erh�ht werden soll
	 */
	public void changeRoom(int index) {
		if((index % 10) >= 100) {
			return;
		}
		
		try {
			game.removeEnemies();
			curRoom = new Room(curRoomIndex + index);
			curRoomIndex += index;
			game.initEnemies();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Zeichnet den momentanen Raum.
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g) {
		curRoom.draw(g);
	}
	
	/**
	 * Gibt den momentanen Raum zur�ck.
	 * @return Der momentane Raum als Room-Objekt
	 */
	public Room getCurrentRoom() {
		return curRoom;
	}
}
