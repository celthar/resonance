package de.celthar.dungeon;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 * Ein Objekt dieser Klasse repr�sentiert einen Speicher f�r Sprite-Objekte.
 * Jedes angeforderte Sprite-Objekt wird in einer HashMap gespeichert und beim
 * erneuten anfordern nur aus der HashMap geholt, statt nochmal komplett in den Speicher geladen.
 * 
 * @author Sascha
 *
 */
public final class SpriteStore {
	private static SpriteStore store = new SpriteStore();
	private HashMap<String, Sprite> sprites = new HashMap<String, Sprite>();
	
	/**
	 * Gibt das SpriteStore-Objekt zur�ck.
	 * @return Der momentan verwendete SpriteStore
	 */
	public static SpriteStore get() {
		return store;
	}
	
	/**
	 * Gibt einen Sprite aus dem SpriteStore zur�ck (sofern vorhanden) oder l�dt das Bild
	 * und erstellt ein zugeh�riges Sprite-Objekt und legt es in der HashMap ab.
	 * @param ref Der Pfad zum Bild
	 * @return Das zugeh�rige Sprite-Objekt
	 */
	public Sprite getSprite(String ref) {
		if(sprites.get(ref) != null) {
			return (Sprite) sprites.get(ref);
		}
		
		BufferedImage sourceImage = null;
		
		try {
			URL url = this.getClass().getClassLoader().getResource(ref);
			
			if(url == null) {
				failed("Can't find: " + ref);
			}
			
			sourceImage = ImageIO.read(url);
		} catch(IOException e) {
			failed("Failed to load: " + ref);
		}
		
		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		Image image = gc.createCompatibleImage(sourceImage.getWidth(), sourceImage.getHeight(), Transparency.BITMASK);
		
		image.getGraphics().drawImage(sourceImage, 0, 0, null);
		
		Sprite sprite = new Sprite(image);
		sprites.put(ref, sprite);
		
		return sprite;
	}
	
	/**
	 * Gibt eine Fehlermeldung mit der entsprechenden Nachricht auf dem <i>System.err</i>-Prinstream aus
	 * @param message Die Nachricht, die ausgegeben werden soll
	 */
	private void failed(String message) {
		System.err.println(message);
		System.exit(0);
	}
}
