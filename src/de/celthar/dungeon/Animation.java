package de.celthar.dungeon;

/**
 * Ein Objekt dieser Klasse stellt eine komplette Animation dar.
 * @author Sascha
 *
 */
public final class Animation {
	private int frames = 0;
	private int curSprite = 0;
	private long passedTime = 0;
	private double timeInterval = 0;
	private Sprite[] sprites = {null};
	
	/**
	 * Erstellt ein Animation-Objekt.
	 * @param sprites Ein Array vom Typ Sprite , welches alle zu nutzenden Bilder
	 * in der richtigen Reihenfolge enth�lt
	 * @param frames Anzahl der Bilder, die in der Animation genutzt werden
	 * @param timeInterval Der Zeit-Intervall nach dem das n�chste Bild der Animation gezeigt wird
	 */
	public Animation(Sprite[] sprites, int frames, double timeInterval) {
		this.sprites = sprites;
		this.frames = frames;
		this.timeInterval = timeInterval;
	}
	
	/**
	 * Gibt das n�chste Bild der Animation zur�ck.
	 * @param delta Die seit dem letzten Aufruf verstrichene Zeit in Millisekunden
	 * @return Das momentane Sprite-Objekt, sofern der Zeit-Intervall noch nicht 
	 * �berschritten wurde, sonst das n�chste Sprite-Objekt der Animation
	 */
	public Sprite nextSprite(double delta) {
		passedTime += delta;
		if(passedTime >= timeInterval) {
			passedTime = 0;
			curSprite++;
			if(curSprite >= frames) {
				curSprite = 0;
			}
		}
		return sprites[curSprite];
	}
}
