package de.celthar.dungeon;

import java.awt.Graphics;

/**
 * Dieses Interface stellt wichtige Methoden f�r Men�s zur Verf�gung.
 * 
 * @author Sascha
 *
 */
public interface Menu {
	/**
	 * Zeichnet das Men�.
	 * @param g Der Grafikkontext, auf dem gezeichnet werden soll
	 */
	public void draw(Graphics g);
	
	/**
	 * Logik-Methode f�r das Men�.
	 */
	public void tick();
}
