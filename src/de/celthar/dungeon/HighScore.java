package de.celthar.dungeon;

import java.io.*;
import java.net.*;

/**
 * Ein Objekt dieser Klasse stellt ein HighScore-Manager dar, der einen Spielernamen
 * und seine Punkte sowie ein Scoreboard verwaltet.
 * 
 * @author Sascha
 *
 */
public final class HighScore {
	private int curPoints = 0;
	private String curPlayer = "";
	
	/**
	 * Ein Konstruktor, der den Spielernamen auf "" setzt und die Punkte auf 0.
	 */
	public HighScore() {
		curPoints = 0;
		curPlayer = "";
	}
	
	/**
	 * Setzt den Spielernamen, der sp�ter im Scoreboard gespeichert wird und die Punkte.
	 * @param curPlayer Der Name des momentanen Spielers.
	 */
	public HighScore(String curPlayer) {
		curPoints = 0;
		this.curPlayer = curPlayer;
	}

	/**
	 * Gibt die momentanen Punkte des Spielers zur�ck.
	 * @return Die momentanen Punkte
	 */
	public int getPoints() {
		return curPoints;
	}

	/**
	 * Erh�ht die Punkte um den �bergebenen int-Wert.
	 * @param points Der Wert, um den die Punkte erh�ht werden
	 */
	public void increasePoints(int points) {
		curPoints += points;
	}
	
	/**
	 * Verringert die Punkte um den �bergebenen int-Wert.
	 * @param points Der Wert, um den die Punkte verringert werden
	 */	
	public void decreasePoints(int points) {
		curPoints -= points;
	}
	
	/**
	 * Liest das Scoreboard aus, f�gt die Punkte des momentanen Spielers hinzu und sortiert dann
	 * absteigend nach den Punkten und speichert dann die zehn h�chsten Werte wieder im Scoreboard.
	 * @throws URISyntaxException Aufgrund der internen Verarbeitung notwendig, sollte niemals auftreten
	 * @throws IOException Tritt auf, wenn die Datei nicht ge�ffnet werden kann
	 */
	public void saveScore() throws URISyntaxException, IOException {
		URL url = this.getClass().getClassLoader().getResource("external/scores/scoreboard.score");
		File f = new File(url.toURI());
		BufferedReader br = new BufferedReader(new FileReader(f));
		String scoreBoard = "HighScore:\n";
		String[] playerNames = new String[11];
		String[] playerScores = new String[11];
		String line = "";
		
		/*
		 * Auslesen des Scoreboards
		 */
		int x = 0;
		while((line = br.readLine()) != null) {
			if(!(line.equals("HighScore:"))) {
				String[] scoreSet = line.split(":");
				playerNames[x] = scoreSet[0];
				playerScores[x] = scoreSet[1];
				x++;
			}
		}
		br.close();
		
		/*
		 * Hinzuf�gen des momentanen Spielers
		 */
		playerNames[10] = curPlayer;
		playerScores[10] = String.valueOf(curPoints);
		
		/*
		 * Sortieren der Spieler nach Punkten
		 */
		for(int r = 0; r < 11; r++) {
			for(int s = r + 1; s < 11; s++) {
				if(Integer.parseInt(playerScores[r]) < Integer.parseInt(playerScores[s])) {
					String tmp = playerScores[r];
					playerScores[r] = playerScores[s];
					playerScores[s] = tmp;
					tmp = playerNames[r];
					playerNames[r] = playerNames[s];
					playerNames[s] = tmp;
				}
			}
		}
		
		/*
		 * Die zehn h�chsten Punkte samt Spielernamen in die Datei schreiben
		 */
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));
		for(x = 0; x < 10; x++) {
			scoreBoard += playerNames[x] + ":" + playerScores[x] + "\n";
		}
		bw.write(scoreBoard);
		bw.close();
	}
	
	/**
	 * Gibt das Scoreboard als zusammenh�ngenden String zur�ck, mit Zeilenumbr�chen.
	 * @return Das Scoreboard als zusammenh�ngender String
	 * @throws URISyntaxException Aufgrund der internen Verarbeitung notwendig, sollte niemals auftreten
	 * @throws IOException Tritt auf, wenn die Datei nicht ge�ffnet werden kann
	 */
	public String getScoreBoard() throws URISyntaxException, IOException {
		URL url = this.getClass().getClassLoader().getResource("external/scores/scoreboard.score");
		File f = new File(url.toURI());
		BufferedReader br = new BufferedReader(new FileReader(f));
		String scoreBoard = "HighScore:\n";
		String line = "";
		
		while((line = br.readLine()) != null) {
			if(!line.equals("HighScore:")) {
				String[] scoreSet = line.split(":");
				scoreBoard += scoreSet[0] + ": " + scoreSet[1] + "\n";
			}
		}
		
		br.close();
		
		return scoreBoard;
	}
}
