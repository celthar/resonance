package de.celthar.dungeon;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Ein Objekt dieser Klasse stellt einen KeyListener zur Verf�gung.
 * 
 * @author Sascha
 *
 */
public final class InputHandler implements KeyListener {
	private static boolean keys[] = new boolean[1024];
	
	/**
	 * Wird aufgerufen, sobald eine Taste gedr�ckt wird. Setzt den entsprechenden Eintrag in
	 * einem Array auf true.
	 */
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode(); 
		if (keyCode > 0 && keyCode < keys.length) {
			keys[keyCode] = true;
		}
	}

	/**
	 * Wird aufgerufen, sobald eine Taste losgelassen wird. Setzt den entsprechenden Eintrag in
	 * einem Array auf false.
	 */
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode(); 
		if (keyCode > 0 && keyCode < keys.length) {
			keys[keyCode] = false;
		}
	}

	/**
	 * Nicht implementiert.
	 */
	public void keyTyped(KeyEvent e) {
		
	}
	
	/**
	 * Gibt den Boolean-Wert aus dem Array zur�ck, an der Stelle, die �bergeben wurde.
	 * @param keyCode Die Stelle des Arrays, die zur�ckgegeben wird. 
	 * @return true, wenn die Taste gedr�ckt ist; sonst false
	 */
	public static boolean getKey(int keyCode) {
		return keys[keyCode];
	}

	/**
	 * Setzt alle Werte des Arrays auf false, um die Tastatureingaben zu l�schen.
	 */
	public static void resetInputs() {
		for(int x = 0; x < keys.length; x++) {
			keys[x] = false;
		}
	}
}
