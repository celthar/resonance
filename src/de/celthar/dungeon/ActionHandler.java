package de.celthar.dungeon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Ein Objekt dieser Klasse dient als ActionListener f�r die JMenuBar des JFrames
 * 
 * @author Sascha
 *
 */
public final class ActionHandler implements ActionListener {
	private Game game = null;
	
	/**
	 * Erstellt ein Objekt dieser Klasse mit einer Referenz auf das Game-Objekt, an das 
	 * der ActionHandler gebunden wird.
	 * @param game Das zugeh�rige Game-Objekt
	 */
	public ActionHandler(Game game) {
		this.game = game;
	}
	
	/**
	 * Wird automatisch aufgerufen, wenn ein Men�-Eintrag angeklickt wird. Pr�ft welcher
	 * Eintrag angeklickt wurde und ruft dementsprechende Methoden auf.
	 */
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		
		if(cmd.equals("New Game")) {
			game.newGame();
		} else if(cmd.equals("Pause Game")) {
			game.pauseGame();
		} else if(cmd.equals("Exit Game")) {
			game.exitGame();
		} else if(cmd.equals("Show ScoreBoard")) {
			try {
				JOptionPane.showMessageDialog(null, game.getHighScore().getScoreBoard(), "About Game", JOptionPane.PLAIN_MESSAGE);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if(cmd.equals("Controls")) {
			JOptionPane.showMessageDialog(null, "W, A, S, D - Move Player\n" +
					"F - Attack", "Controls", JOptionPane.PLAIN_MESSAGE);
		}	else if(cmd.equals("About Game")) {
			JOptionPane.showMessageDialog(null, "Created by: Sascha Riedl\n" +
					"\n" +
					"Credit to:\n" +
					"   Music:\n" +
					"      Zefz (opengameart.org)\n" +
					"   Font:\n" +
					"      Andrew Tyler (dafont.com)", "About Game", JOptionPane.PLAIN_MESSAGE);
		}
	}
}
